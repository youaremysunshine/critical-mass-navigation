import { navigation } from './data/navigation.js';
import './clock.js'

window.addEventListener('load', () => {
  setNav()
})

window.addEventListener('resize', () => {
  underline.style.transition = 'none'
  reposition(activeCity)
  underline.style.transition = 'left 0.2s linear'
})

let citiesArray = navigation.cities;

let times = {
  cupertino: moment().tz('America/Los_Angeles'),
  newYorkCity: moment().tz('America/New_York'),
  london: moment().tz('Europe/London'),
  amsterdam: moment().tz('Europe/Amsterdam'),
  tokyo: moment().tz('Asia/Tokyo'),
  hongKong: moment().tz('Asia/Hong_Kong'),
  sydney: moment().tz('Australia/Sydney')
}

// Create nav
const nav = document.getElementById('navigation')
const underline = document.createElement('div')
underline.setAttribute('id', 'underline')
nav.appendChild(underline)
let activeCity = document.getElementsByClassName('active')

const reposition = (city) => {
  setUnderlineSize(city[0])
}

const active = (city) => {
  if (activeCity.length === 1) {
    activeCity[0].classList.remove('active')
  }

  city.classList.add('active')
  
  setUnderlineSize(city)
  let timeZone = city.getAttribute('id').replace(/-([a-z])/g, function (g) { return g[1].toUpperCase() })
  startTime(times[timeZone])
}

const setUnderlineSize = (city) => {
  let offset = city.getBoundingClientRect().left - nav.getBoundingClientRect().left
  
  underline.style.width = `${city.getBoundingClientRect().width}px`
  underline.style.left = `${offset}px`
  underline.style.opacity = 1
}

const setNav = () => {
  for (var i = 0; i < citiesArray.length; i++) {
    const listItem = document.createElement("li")
    listItem.classList.add('city-container')
    nav.appendChild(listItem)
    
    const city = document.createElement('a')
    city.href = '#'
    city.setAttribute('id', citiesArray[i].section)
    city.innerHTML = citiesArray[i].label
    listItem.appendChild(city)

    city.addEventListener('click', () => {
      active(city)
    })
  }
}

const startTime = (timezone) => {
  document.getElementById('time').innerHTML = timezone.format('LLLL')
}
